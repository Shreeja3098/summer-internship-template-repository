## Alpha Team - Dynamic PERSONALIZED UI CREATION

The participants are required to fork this repository and create a private Gitlab repository under their own username (Single repository per team). The following created sections in this README.md need to be duly filled, highlighting the denoted points for the solution/implementation. Please feel free to create further sub-sections in this markdown, the idea is to understand the gist of the components in a singular document.

### Project Overview

Our project is helpful for those organization that lack the knowledge of creating a web page . Hence dynamic UI helped us achieve thie goal to help these organizations.
----------------------------------

A brief description of 
* What problem did the team try to solve
* What is the proposed solution

### Solution Description
----------------------------------
We have approacged the problem by using react rnd and react dnd to resize,drag and drop the widgets and thus exporting the final web page .

#### Architecture Diagram
![Screenshot__7_](/uploads/81413b4e7f01d07b934bf9315b59dae5/Screenshot__7_.png)

#### Technical Description

An overview of 
* What technologies/versions were used
* Setup/Installations required to run the solution
* Instructions to run the submitted code

### Team Members
----------------------------
Shreeja Majumdar: 1706565@kiit.ac.in
Varsha:1706187@kiit.ac.in
Khushbu Sinha :1728116@kiit.ac.in
Hanee Rai : 1706323@kiit.ac.in
